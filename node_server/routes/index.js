var express = require('express');
var router = express.Router();
var get_ip = require('ipware')().get_ip;

/* GET home page. */
router.get('/', function (req, res, next) {
    var ip = get_ip(req, right_most_proxy=true)
    res.json({
        message: "SUCCESS : YOU HAVE ACCESS TO ADDRESS",
        ip: ip
    })
});

module.exports = router;

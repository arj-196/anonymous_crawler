import java.io.FileWriter
import simple_request._

object MakeARequest {

  def main(args: Array[String]) {
    val filename = "results/server_response.csv"
    val req = new Request()

    println("making a simple web request")
    println("logging response to file" + filename)

    val fw = new FileWriter(filename, true)
    val NEWLINE = "\n"

    for (a <- 1 to 10) {
      val html = req.get("http://51.254.223.26:9966/")
      fw.write(html + NEWLINE)
    }
    fw.flush()
    fw.close()

    println("done")
  }

}